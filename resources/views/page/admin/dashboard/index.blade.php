@extends('layouts.index')

@section('title', 'Dashboard')

@section('body')
    @include('partials.admin.navbar', ['title' => 'Dashboard'])

    @include('partials.admin.sidebar', ['active' => 'dashboard'])
@endsection
